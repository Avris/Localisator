<?php
namespace Avris\Localisator;

use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * @codeCoverageIgnore
 */
final class LocalisatorTwig extends AbstractExtension implements GlobalsInterface
{
    /** @var LocalisatorInterface */
    private $localisator;

    public function __construct(LocalisatorInterface $localisator)
    {
        $this->localisator = $localisator;
    }

    public function getGlobals(): array
    {
        return [
            'locales' => $this->localisator->getOrder()->getSupported(),
        ];
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('l', function ($word, $replacements = []) {
                return $this->localisator->get($word, $replacements);
            }, ['is_safe' => ['html']]),
            new TwigFunction('currentLocale', function () {
                return $this->localisator->getOrder()->getCurrent();
            }),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('l', function ($word, $replacements = []) {
                return $this->localisator->get($word, $replacements);
            }, ['is_safe' => ['html']])
        ];
    }
}
