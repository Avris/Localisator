<?php
namespace Avris\Localisator\Transformer;

use Avris\Dispatcher\Event;

class TranslationTransformEvent extends Event
{
    /** @var string */
    private $namespace;

    /** @var string */
    private $word;

    /** @var string[] */
    private $replacements;

    /** @var string|null */
    private $translated;

    public function __construct(string $namespace, string $word, array $replacements, ?string $translated)
    {
        $this->setNamespace($namespace);
        $this->setWord($word);
        $this->setReplacements($replacements);
        $this->translated = $translated;
    }

    private function clearReplacements(array $replacements)
    {
        $new = [];
        foreach ($replacements as $key => $value) {
            $new[preg_match('#%\w+%#', $key) ? $key : '%' . $key . '%'] = $value;
        }

        return $new;
    }

    public function getName(): string
    {
        return 'translationTransform';
    }

    public function getNamespace(): string
    {
        return $this->namespace;
    }

    public function setNamespace(string $namespace): self
    {
        $this->namespace = $namespace;

        return $this;
    }

    public function getWord(): string
    {
        return $this->word;
    }

    public function setWord(string $word): self
    {
        $this->word = $word;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getReplacements(): array
    {
        return $this->replacements;
    }

    public function setReplacements(array $replacements): self
    {
        $this->replacements = $this->clearReplacements($replacements);

        return $this;
    }

    public function getTranslated(): ?string
    {
        return $this->translated;
    }

    public function hasTranslation(): bool
    {
        return $this->translated !== null;
    }

    public function setTranslated($translated): self
    {
        $this->translated = $translated;

        return $this;
    }

    public function setValue($value): Event
    {
        $this->translated = $value;

        return $this;
    }

    public function getValue()
    {
        return $this->translated;
    }
}
