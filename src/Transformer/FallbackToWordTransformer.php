<?php
namespace Avris\Localisator\Transformer;

class FallbackToWordTransformer extends AbstractTransformer
{
    public function transform(TranslationTransformEvent $event)
    {
        if (!$event->hasTranslation()) {
            return $event->getWord();
        }
    }

    protected function getPriority(): int
    {
        return 20;
    }
}
