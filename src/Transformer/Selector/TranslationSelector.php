<?php
namespace Avris\Localisator\Transformer\Selector;

interface TranslationSelector
{
    public function getName(): string;

    public function select(array $replacements, array $versions): string;
}
