<?php
namespace Avris\Localisator\Transformer\Selector;

final class CountVersionSelector implements TranslationSelector
{
    public function getName(): string
    {
        return '';
    }

    public function select(array $replacements, array $versions): string
    {
        $count = isset($replacements['%count%']) ? abs((int) $replacements['%count%']) : 1;

        foreach ($versions as $i => $version) {
            list ($requirement, $translation) = preg_match('#^\{([\d,-]*)\}(.*)#', $version, $matches)
                ? [$matches[1], trim($matches[2])]
                : [$i == 0 ? '1' : '-0,2-', trim($version)];

            if ($this->matches($count, $requirement)) {
                return $translation;
            }
        }

        return '';
    }

    private function matches(int $count, string $requirement): bool
    {
        foreach (explode(',', $requirement) as $range) {
            list($min, $max) = explode('-', $range) + [1 => null];

            if ($this->rangeMatches($count, $min, $max)) {
                return true;
            }
        }

        return false;
    }

    private function rangeMatches($count, $min, $max)
    {
        if (!isset($max)) {
            if ($min === '') {
                return true;
            }

            return $count == $min;
        }

        if ($min !== '' && $max === '') {
            return $count >= $min;
        }

        if ($min === '' && $max !== '') {
            return $count <= $max;
        }

        return $count >= $min && $count <= $max;
    }
}
