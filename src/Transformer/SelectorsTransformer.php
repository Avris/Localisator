<?php
namespace Avris\Localisator\Transformer;

use Avris\Localisator\Transformer\Selector\TranslationSelector;

class SelectorsTransformer extends AbstractTransformer
{
    /** @var TranslationSelector[] */
    private $selectors = [];

    public function __construct(array $translationSelectors)
    {
        foreach ($translationSelectors as $selector) {
            $this->selectors[$selector->getName()] = $selector;
        }
    }

    public function transform(TranslationTransformEvent $event)
    {
        if ($event->hasTranslation() && preg_match('#^<([A-Za-z0-9]*)>(.*)$#', $event->getTranslated(), $matches)) {
            list(,$selector,$versions) = $matches;
            if (isset($this->selectors[$selector])) {
                $versions = array_map('trim', explode('|', $versions));

                return $this->selectors[$selector]->select($event->getReplacements(), $versions);
            }
        }
    }

    protected function getPriority(): int
    {
        return -10;
    }
}
