<?php
namespace Avris\Localisator\Transformer;

class ReplacementsTransformer extends AbstractTransformer
{
    public function transform(TranslationTransformEvent $event)
    {
        return $event->hasTranslation()
            ? strtr($event->getTranslated(), $event->getReplacements())
            : null;
    }

    protected function getPriority(): int
    {
        return -20;
    }
}
