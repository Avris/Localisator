<?php
namespace Avris\Localisator\Transformer;

use Avris\Dispatcher\EventSubscriberInterface;

abstract class AbstractTransformer implements EventSubscriberInterface
{
    public function getSubscribedEvents(): iterable
    {
        yield 'translationTransform:' . $this->getPriority() => [$this, 'transform'];
    }

    abstract public function transform(TranslationTransformEvent $event);

    abstract protected function getPriority(): int;
}
