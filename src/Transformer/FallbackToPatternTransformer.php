<?php
namespace Avris\Localisator\Transformer;

class FallbackToPatternTransformer extends AbstractTransformer
{
    /** @var string[] */
    private $patterns = [];

    public function __construct(array $patterns)
    {
        $this->patterns = $patterns;
    }

    public function transform(TranslationTransformEvent $event)
    {
        if ($event->hasTranslation()) {
            return null;
        }

        foreach ($this->patterns[$event->getNamespace()] ?? [] as $pattern) {
            if (preg_match('#' . $pattern . '#', $event->getWord(), $matches)) {
                return ucfirst($matches[1]);
            }
        }

        return null;
    }

    protected function getPriority(): int
    {
        return 30;
    }
}
