<?php
namespace Avris\Localisator\Transformer;

use Avris\Localisator\LocalisatorInterface;

class NestedTransformer extends AbstractTransformer
{
    /** @var LocalisatorInterface */
    private $localisator;

    public function __construct(LocalisatorInterface $localisator)
    {
        $this->localisator = $localisator;
    }

    public function transform(TranslationTransformEvent $event)
    {
        if (!$event->hasTranslation()) {
            return null;
        }

        return preg_replace_callback('#\[\[(.+)\]\]#Ui', function ($matches) use ($event) {
            return $this->localisator->get(strtr($matches[1], $event->getReplacements()), $event->getReplacements());
        }, $event->getTranslated());
    }

    protected function getPriority(): int
    {
        return 10;
    }
}
