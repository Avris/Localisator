<?php
namespace Avris\Localisator;

use Avris\Micrus\Bootstrap\ModuleInterface;
use Avris\Micrus\Bootstrap\ModuleTrait;

final class LocalisatorModule implements ModuleInterface
{
    use ModuleTrait;
}
