<?php
namespace Avris\Localisator\Handler;

use Avris\Bag\Bag;
use Avris\Container\ContainerInterface;
use Avris\Micrus\Bootstrap\ConfigExtension;
use Avris\Micrus\Controller\ControllerInterface;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Micrus\Controller\Routing\Model\CompiledRoute;
use Avris\Micrus\Controller\Routing\Model\Route;
use Avris\Micrus\Controller\Routing\Service\RouterExtension;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;

final class UrlLocaleHandler implements ControllerInterface, RouterExtension, ConfigExtension
{
    /** @var ContainerInterface */
    private $container;

    /** @var string */
    private $supported;

    public function __construct(ContainerInterface $container, Bag $configLocalisation_supported)
    {
        $this->container = $container;
        $this->supported = implode('|', $configLocalisation_supported->keys()) ?? 'en';
    }

    public function add(Route $route): Route
    {
        if ($route->getOption('immutable')) {
            return $route;
        }

        $route->setPattern('/{_locale}' . $route->getPattern());
        $route->addRequirement('_locale', $this->supported);

        return $route;
    }

    public function generate(string $name, ?CompiledRoute $route, array $params = []): array
    {
        if ($name === 'changeLocale') {
            $routeMatch = $this->container->get(RequestProviderInterface::class)->getRouteMatch();
            $router = $this->container->get(RouterInterface::class);
            $url = $router->generate(
                $routeMatch ? $routeMatch->getRoute()->getName() : $router->getDefaultRoute(),
                array_merge($routeMatch ? $routeMatch->getTags() : [], ['_locale' => $params['locale']])
            );

            return [$url, $route, $params];
        }

        if (!$route || $route->getOption('immutable')) {
            return [null, $route, $params];
        }

        $tagNames = $route->getTagNames();

        if (in_array('_locale', $tagNames) && !array_key_exists('_locale', $params)) {
            $params['_locale'] = (string) $this->container->get(LocaleOrderProviderInterface::class)->getCurrent();
        }

        return [null, $route, $params];
    }

    public function determineLocaleAction(
        RequestInterface $request,
        RouterInterface $router,
        LocaleOrderProviderInterface $orderProvider
    ) {
        $supported = $orderProvider->getSupported();

        $bestLocale = (string) $orderProvider->getOrder()->first();
        foreach ($request->getHeaders()->getLanguage()->getSorted() as $locale) {
            if ($supported->has($locale)) {
                $bestLocale = $locale;
                break;
            }
        }

        return new RedirectResponse($router->generate('home', ['_locale' => strtr($bestLocale, ['-' => '_'])]));
    }

    /**
     * @codeCoverageIgnore
     */
    public function extendConfig(): array
    {
        return [
            'routing' => [
                'determineLocale' => '/ -> ' . __CLASS__ . '/determineLocale {"immutable":true}',
            ],
        ];
    }
}
