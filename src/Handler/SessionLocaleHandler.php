<?php
namespace Avris\Localisator\Handler;

use Avris\Micrus\Bootstrap\ConfigExtension;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Micrus\Controller\ControllerInterface;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Micrus\Exception\Http\NotFoundHttpException;
use Avris\Localisator\Order\LocaleOrderProviderInterface;

final class SessionLocaleHandler implements ControllerInterface, ConfigExtension
{
    public function changeLocaleAction(
        RequestInterface $request,
        SessionInterface $session,
        RouterInterface $router,
        LocaleOrderProviderInterface $orderProvider,
        EventDispatcherInterface $dispatcher,
        string $locale
    ): RedirectResponse {
        if (!$orderProvider->getSupported()->has($locale)) {
            throw new NotFoundHttpException(sprintf('Unsupported locale %s', $locale));
        }

        list($shouldChange, $newLocale) = $dispatcher->trigger(
            new LocaleChangedEvent($session->get('_locale'), $locale)
        );

        if ($shouldChange) {
            $session->set('_locale', $newLocale);
        }

        return new RedirectResponse(
            $request->getHeaders()->get('referer') ?: $router->generate(RouterInterface::DEFAULT_ROUTE)
        );
    }

    /**
     * @codeCoverageIgnore
     */
    public function extendConfig(): array
    {
        return [
            'routing' => [
                'changeLocale' => '/locale/{<[a-z]{2}(?:_[A-Z]{2})?>:locale} -> ' . __CLASS__ . '/changeLocale',
            ],
        ];
    }
}
