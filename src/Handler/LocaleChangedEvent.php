<?php
namespace Avris\Localisator\Handler;

use Avris\Dispatcher\Event;

final class LocaleChangedEvent extends Event
{
    /** @var string|null */
    private $oldLocale;

    /** @var string */
    private $newLocale;

    /** @var bool */
    private $shouldChange = true;

    public function __construct(?string $oldLocale, string $newLocale)
    {
        $this->oldLocale = $oldLocale;
        $this->newLocale = $newLocale;
    }

    public function getOldLocale(): ?string
    {
        return $this->oldLocale;
    }

    public function getNewLocale(): string
    {
        return $this->newLocale;
    }

    public function setNewLocale(string $newLocale): self
    {
        $this->newLocale = $newLocale;

        return $this;
    }

    public function shouldChange(): bool
    {
        return $this->shouldChange;
    }

    public function setShouldChange(bool $shouldChange): self
    {
        $this->shouldChange = $shouldChange;

        return $this;
    }

    public function getName(): string
    {
        return 'localeChanged';
    }

    public function setValue($value): Event
    {
        list($this->shouldChange, $this->newLocale) = $value;

        return $this;
    }

    public function getValue()
    {
        return [$this->shouldChange, $this->newLocale];
    }
}
