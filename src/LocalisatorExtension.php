<?php
namespace Avris\Localisator;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;
use Avris\Dispatcher\EventDispatcher;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Localisator\Locale\LocaleInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Localisator\Order\SimpleLocaleOrderProvider;
use Avris\Localisator\Provider\DirsTranslationProvider;
use Avris\Localisator\Provider\FileReader\JsonFileReader;
use Avris\Localisator\Provider\FileReader\PhpFileReader;
use Avris\Localisator\Provider\FileReader\YamlFileReader;
use Avris\Localisator\Provider\TranslationProviderInterface;
use Avris\Localisator\Transformer\NestedTransformer;
use Avris\Localisator\Transformer\ReplacementsTransformer;
use Avris\Localisator\Transformer\Selector\CountVersionSelector;
use Avris\Localisator\Transformer\SelectorsTransformer;

/**
 * @codeCoverageIgnore
 */
final class LocalisatorExtension implements ContainerBuilderExtension
{
    /** @var LocaleInterface|string */
    private $locale;

    /** @var array|string[] */
    private $dirs;

    /**
     * @param string|LocaleInterface $locale
     * @param string[] $dirs
     */
    public function __construct($locale, $dirs = [])
    {
        $this->locale = $locale;
        $this->dirs = $dirs;
    }

    public function extend(ContainerInterface $container)
    {
        $container->setDefinition(LocalisatorInterface::class, [
            'class' => Localisator::class,
            'arguments' => [
                '$translationProvider' => '@' . TranslationProviderInterface::class,
                '$orderProvider' => '@' . LocaleOrderProviderInterface::class,
                '$dispatcher' => '@' . EventDispatcherInterface::class,
            ],
            'public' => true,
        ]);

        $container->setDefinition(TranslationProviderInterface::class, [
            'class' => DirsTranslationProvider::class,
            'arguments' => [
                '$dirs' => '#translationDir',
                '$fileReaders' => '#fileReader',
            ],
        ]);

        foreach ($this->dirs as $dir) {
            $container->setDefinition('translationDir' . md5($dir), [
                'resolve' => $dir,
                'tags' => ['translationDir'],
            ]);
        }

        $container->setDefinition(LocaleOrderProviderInterface::class, [
            'class' => SimpleLocaleOrderProvider::class,
            'arguments' => [
                '$locale' => $this->locale,
            ]
        ]);

        $container->setDefinition(CountVersionSelector::class, ['tags' => 'translationSelector']);

        $container->setDefinition(PhpFileReader::class, ['tags' => 'fileReader']);
        $container->setDefinition(YamlFileReader::class, ['tags' => 'fileReader']);
        $container->setDefinition(JsonFileReader::class, ['tags' => 'fileReader']);

        $container->setDefinition(ReplacementsTransformer::class, ['tags' => 'subscriber']);
        $container->setDefinition(SelectorsTransformer::class, [
            'arguments' => [
                '$translationSelectors' => '#translationSelector',
            ],
            'tags' => ['subscriber'],
        ]);
        $container->setDefinition(NestedTransformer::class, [
            'arguments' => [
                '$localisator' => '@' . LocalisatorInterface::class,
            ],
            'tags' => ['subscriber'],
        ]);
    }
}
