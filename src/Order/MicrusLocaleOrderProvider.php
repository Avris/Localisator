<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Localisator\Locale\LocaleInterface;

final class MicrusLocaleOrderProvider implements EventSubscriberInterface, LocaleOrderProviderInterface
{
    /** @var RequestProviderInterface */
    private $requestProvider;

    /** @var SessionInterface */
    private $session;

    /** @var string */
    private $fallback;

    /** @var Bag|string[] */
    private $supported;

    /** @var LocaleOrder */
    private $order;

    public function __construct(
        RequestProviderInterface $requestProvider,
        SessionInterface $session,
        Bag $configLocalisation_supported,
        string $fallback = 'en'
    ) {
        if ($configLocalisation_supported->isEmpty()) {
            $configLocalisation_supported->set('en', 'English');
        }

        $this->requestProvider = $requestProvider;
        $this->session = $session;
        $this->supported = $configLocalisation_supported;
        $this->fallback = $fallback;
    }

    public function getOrder(): LocaleOrder
    {
        return $this->order ?: $this->order = $this->rebuild();
    }

    public function rebuild($force = null): LocaleOrder
    {
        $this->order = new LocaleOrder($this->supported);

        if ($force) {
            $this->order->add($force);
        }

        $routeMatch = $this->requestProvider->getRouteMatch();
        if ($routeMatch && isset($routeMatch->getTags()['_locale'])) {
            $this->order->add($routeMatch->getTags()['_locale']);
        }

        $this->order->add($this->session->get('_locale'), true);

        if ($headerLanguages = $this->requestProvider->getRequest()->getHeaders()->getLanguage()) {
            $this->order->addMultiple($headerLanguages->getSorted());
        }

        $this->order->add($this->fallback, true);

        return $this->order;
    }

    public function getSupported(): Bag
    {
        return $this->supported;
    }

    public function getCurrent(): LocaleInterface
    {
        return $this->getOrder()->main();
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'request' => function () {
            return $this->rebuild();
        };
    }
}
