<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use Avris\Localisator\Locale\LocaleInterface;

interface LocaleOrderProviderInterface
{
    public function getOrder(): LocaleOrder;

    public function rebuild($force = null): LocaleOrder;

    public function getSupported(): Bag;

    public function getCurrent(): LocaleInterface;
}
