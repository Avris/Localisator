<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use Avris\Localisator\Locale\Locale;
use Avris\Localisator\Locale\LocaleInterface;

final class SimpleLocaleOrderProvider implements LocaleOrderProviderInterface
{
    /** @var string */
    private $locale;

    /** @var LocaleOrder */
    private $order;

    public function __construct($locale)
    {
        $this->locale = $locale;
        $this->rebuild($locale);
    }

    public function getOrder(): LocaleOrder
    {
        return $this->order;
    }

    public function rebuild($force = null): LocaleOrder
    {
        $this->order = new LocaleOrder($this->getSupported());
        $this->order->add($force);

        return $this->order;
    }

    public function getSupported(): Bag
    {
        return new Bag([
            $this->locale => $this->locale,
        ]);
    }

    public function getCurrent(): LocaleInterface
    {
        return new Locale($this->locale);
    }
}
