<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use Avris\Bag\Set;
use Avris\Localisator\Locale\Locale;
use Avris\Localisator\Locale\LocaleInterface;

final class LocaleOrder extends Set
{
    /** @var LocaleInterface[] */
    protected $values = [];

    /** @var Bag */
    private $supportedLocales;

    /** @var Set */
    private $supportedLanguages;

    public function __construct(Bag $supported, array $values = [], callable $callback = null)
    {
        $this->supportedLocales = $supported;
        $this->supportedLanguages = new Set($supported->keys(), function ($v) {
            return substr($v, 0, 2);
        });
        parent::__construct($values, $callback);
    }

    public function add($value, $force = false): Set
    {
        if ($value && !in_array($value, $this->values, true)) {
            $locale = new Locale($value);

            if ($force || $this->supportedLanguages->has($locale->getLanguage())) {
                $this->values[] = $locale;
            }

            if (!$locale->isGeneral()) {
                $this->add($locale->getLanguage());
            }
        }

        return $this;
    }

    /**
     * @return string[]
     */
    public function getList(): array
    {
        return array_map(function (Locale $locale) {
            return (string) $locale;
        }, $this->values);
    }

    public function main(): LocaleInterface
    {
        foreach ($this->values as $locale) {
            if ($this->supportedLocales->has((string) $locale)) {
                return $locale;
            }
        }

        return $this->first();
    }
}
