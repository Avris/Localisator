<?php
namespace Avris\Localisator\Locale;

final class Locale implements LocaleInterface
{
    /** @var string */
    private $language;

    /** @var string */
    private $country;

    /**
     * @param string $code format: xx or xx_YY
     */
    public function __construct(string $code)
    {
        $code = explode('_', $code);
        $this->language = $code[0];
        $this->country = $code[1] ?? null;
    }

    public function __toString(): string
    {
        return $this->country ? $this->language . '_' . $this->country : $this->language;
    }

    public function getLanguage(): string
    {
        return $this->language;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function isGeneral(): bool
    {
        return $this->country === null;
    }
}
