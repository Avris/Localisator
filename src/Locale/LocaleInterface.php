<?php
namespace Avris\Localisator\Locale;

interface LocaleInterface
{
    public function getLanguage(): string;

    public function getCountry(): ?string;

    public function isGeneral(): bool;

    public function __toString(): string;
}
