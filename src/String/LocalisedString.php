<?php
namespace Avris\Localisator\String;

use Avris\Localisator\LocalisatorInterface;

final class LocalisedString implements \JsonSerializable
{
    /** @var LocalisatorInterface */
    private static $localisator;

    /** @var string */
    private $word;

    /** @var string */
    private $replacements;

    public function __construct(string $word, array $replacements = [])
    {
        $this->word = $word;
        $this->replacements = $replacements;
    }

    public static function setLocalisator(LocalisatorInterface $localisator = null)
    {
        static::$localisator = $localisator;
    }

    public static function getLocalisator(): ?LocalisatorInterface
    {
        return self::$localisator;
    }

    public function getLocalised(): ?string
    {
        return static::$localisator
            ? static::$localisator->get($this->word, $this->replacements)
            : strtr($this->word, $this->replacements);
    }

    public function exists()
    {
        return static::$localisator
            ? static::$localisator->has($this->word)
            : true;
    }

    public function __toString()
    {
        return (string) $this->getLocalised();
    }

    public function jsonSerialize()
    {
        return $this->getLocalised();
    }
}
