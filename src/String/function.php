<?php
use Avris\Localisator\String\LocalisedString;

if (!function_exists('l')) {
    /**
     * @codeCoverageIgnore
     */
    function l(string $word, array $replacements = []): LocalisedString
    {
        return new LocalisedString($word, $replacements);
    }
}
