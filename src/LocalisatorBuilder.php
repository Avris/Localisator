<?php
namespace Avris\Localisator;

use Avris\Container\ContainerAssistedBuilder;
use Avris\Dispatcher\EventDispatcher;
use Avris\Dispatcher\EventDispatcherInterface;

/**
 * @codeCoverageIgnore
 */
final class LocalisatorBuilder extends ContainerAssistedBuilder
{
    /** @var EventDispatcher */
    private $dispatcher;

    /** @var bool */
    private $subscribersRegistered = false;

    public function __construct()
    {
        parent::__construct();

        $this->dispatcher = new EventDispatcher;
        $this->container->set(EventDispatcherInterface::class, $this->dispatcher);
    }

    public function build(string $service)
    {
        if (!$this->subscribersRegistered) {
            foreach ($this->container->getByTag('subscriber') as $subscriber) {
                $this->dispatcher->registerSubscriber($subscriber);
            }
        }

        return parent::build($service);
    }
}
