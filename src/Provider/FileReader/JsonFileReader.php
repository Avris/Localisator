<?php
namespace Avris\Localisator\Provider\FileReader;

final class JsonFileReader implements FileReader
{
    public function read(string $filename): array
    {
        return json_decode(file_get_contents($filename), true);
    }

    public function getExtension(): string
    {
        return 'json';
    }
}
