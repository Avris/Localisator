<?php
namespace Avris\Localisator\Provider\FileReader;

interface FileReader
{
    public function read(string $filename): array;

    public function getExtension(): string;
}
