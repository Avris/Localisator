<?php
namespace Avris\Localisator\Provider\FileReader;

final class YamlFileReader implements FileReader
{
    public function read(string $filename): array
    {
        return \Spyc::YAMLLoad($filename);
    }

    public function getExtension(): string
    {
        return 'yml';
    }
}
