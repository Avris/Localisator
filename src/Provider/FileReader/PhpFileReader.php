<?php
namespace Avris\Localisator\Provider\FileReader;

final class PhpFileReader implements FileReader
{
    public function read(string $filename): array
    {
        return require $filename;
    }

    public function getExtension(): string
    {
        return 'php';
    }
}
