<?php
namespace Avris\Localisator\Provider;

interface TranslationProviderInterface
{
    public function get(string $namespace, string $word, string $locale): ?string;

    /**
     * @return iterable|string[] "$namespace:$key"
     */
    public function keys(): iterable;
}
