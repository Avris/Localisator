<?php
namespace Avris\Localisator\Provider;

use Avris\Bag\Bag;
use Avris\Bag\BagHelper;
use Avris\Localisator\Provider\FileReader\FileReader;

final class DirsTranslationProvider implements TranslationProviderInterface
{
    /** @var string[] */
    private $dirs;

    /** @var FileReader[] */
    private $fileReaders;

    /** @var Bag[][] */
    private $locales = null;

    public function __construct($dirs, array $fileReaders)
    {
        $this->dirs = BagHelper::toArray($dirs);
        $this->fileReaders = $fileReaders;
    }

    private function warmup()
    {
        if ($this->locales !== null) {
            return;
        }

        $this->locales = [];
        foreach ($this->dirs as $dir) {
            foreach (glob($dir . '/*.*') as $filename) {
                $pattern = '#/([A-Za-z0-9-_]+)\.([a-z][a-z](?:_[A-Z][A-Z])?)\.([^/.]+)$#';
                if (!preg_match($pattern, $filename, $matches)) {
                    continue;
                }

                list(, $namespace, $locale, $extension) = $matches;

                foreach ($this->fileReaders as $fileProvider) {
                    if ($fileProvider->getExtension() === $extension) {
                        $this->locales[$locale][$namespace] = new Bag($fileProvider->read($filename));
                    }
                }
            }
        }
    }

    public function get(string $namespace, string $word, string $locale): ?string
    {
        $this->warmup();

        return isset($this->locales[$locale][$namespace])
            ? $this->locales[$locale][$namespace]->getDeep($word)
            : null;
    }
    
    public function keys(): iterable
    {
        $this->warmup();

        foreach ($this->locales as $locale => $namespaceTranslations) {
            foreach ($namespaceTranslations as $namespace => $translations) {
                foreach ($translations->flatten() as $key => $value) {
                    yield sprintf('%s:%s', $namespace, $key);
                }
            }
        }
    }
}
