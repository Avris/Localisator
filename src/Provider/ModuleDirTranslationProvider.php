<?php
namespace Avris\Localisator\Provider;

use Avris\Localisator\Provider\FileReader\FileReader;
use Avris\Micrus\Bootstrap\ModuleInterface;

final class ModuleDirTranslationProvider implements TranslationProviderInterface
{
    /** @var ModuleInterface[] */
    private $modules;

    /** @var FileReader[] */
    private $fileReaders;

    /** @var TranslationProviderInterface[] */
    private $providers;

    public function __construct(array $modules, array $fileReaders)
    {
        $this->modules = $modules;
        $this->fileReaders = $fileReaders;
    }

    private function warmup()
    {
        if ($this->providers !== null) {
            return;
        }

        $this->providers = [];

        /** @var ModuleInterface $module */
        foreach (array_reverse($this->modules) as $module) {
            if (is_dir($dir = $module->getDir() . '/translations')) {
                $this->providers[] = new DirsTranslationProvider($dir, $this->fileReaders);
            }
        }
    }

    public function get(string $namespace, string $word, string $locale): ?string
    {
        $this->warmup();

        foreach ($this->providers as $provider) {
            $translated = $provider->get($namespace, $word, $locale);
            if ($translated !== null) {
                return $translated;
            }
        }

        return null;
    }

    public function keys(): iterable
    {
        $this->warmup();
        foreach ($this->providers as $provider) {
            yield from $provider->keys();
        }
    }
}
