<?php
namespace Avris\Localisator\Provider;

use Avris\Bag\Set;
use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Psr\Cache\CacheItemPoolInterface;

final class CacheTranslationProvider implements TranslationProviderInterface, EventSubscriberInterface
{
    /** @var TranslationProviderInterface */
    private $provider;

    /** @var CacheItemPoolInterface */
    private $cache;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    public function __construct(
        TranslationProviderInterface $provider,
        CacheItemPoolInterface $cachePool,
        LocaleOrderProviderInterface $orderProvider
    ) {
        $this->provider = $provider;
        $this->cache = $cachePool;
        $this->orderProvider = $orderProvider;
    }

    public function get(string $namespace, string $word, string $locale): ?string
    {
        $item = $this->cache->getItem(sprintf('translation-%s-%s-%s', $locale, $namespace, $word));
        if (!$item->isHit()) {
            $item->set($this->provider->get($namespace, $word, $locale));
            $item->expiresAfter(null);
            $this->cache->save($item);
        }

        return $item->get();
    }

    public function keys(): iterable
    {
        yield from $this->provider->keys();
    }

    public function warmup()
    {
        $locales = new Set();
        foreach ($this->orderProvider->getSupported() as $locale => $name) {
            $locales->add($locale);
            if (strlen($locale) === 5) {
                $locales->add(substr($locale, 0, 2));
            }
        }

        foreach ($this->keys() as $key) {
            foreach ($locales as $locale) {
                $item = $this->cache->getItem(sprintf('translation-%s-%s', $locale, strtr($key, [':' => '-'])));
                list($namespace, $word) = explode(':', $key, 2);
                $item->set($this->provider->get($namespace, $word, $locale));
                $this->cache->saveDeferred($item);
            }
        }

        $this->cache->commit();
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'cacheWarmup' => [$this, 'warmup'];
    }
}
