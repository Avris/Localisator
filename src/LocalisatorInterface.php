<?php
namespace Avris\Localisator;

use Avris\Localisator\Order\LocaleOrderProviderInterface;

interface LocalisatorInterface
{
    public function get(string $word, array $replacements = []): ?string;

    public function has(string $word): bool;

    public function getOrder(): LocaleOrderProviderInterface;
}
