<?php
namespace Avris\Localisator;

use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Dispatcher\EventSubscriberInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Localisator\Provider\TranslationProviderInterface;
use Avris\Localisator\String\LocalisedString;
use Avris\Localisator\Transformer\TranslationTransformEvent;

final class Localisator implements EventSubscriberInterface, LocalisatorInterface
{
    /** @var TranslationProviderInterface */
    private $translationProvider;

    /** @var LocaleOrderProviderInterface */
    private $orderProvider;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    public function __construct(
        TranslationProviderInterface $translationProvider,
        LocaleOrderProviderInterface $orderProvider,
        EventDispatcherInterface $dispatcher
    ) {
        $this->translationProvider = $translationProvider;
        $this->orderProvider = $orderProvider;
        $this->dispatcher = $dispatcher;
    }

    public function get(string $word, array $replacements = []): ?string
    {
        list($namespace, $word) = $this->splitWordNamespace($word);

        $translated = $this->findWord($word, $namespace);

        $event = new TranslationTransformEvent($namespace, $word, $replacements, $translated);

        return $this->dispatcher->trigger($event);
    }

    public function has(string $word): bool
    {
        list($namespace, $word) = $this->splitWordNamespace($word);

        return $this->findWord($word, $namespace) !== null;
    }

    public function __invoke(string $word, array $replacements = []): ?string
    {
        return $this->get($word, $replacements);
    }

    private function splitWordNamespace(string $word): array
    {
        return strpos($word, ':') === false
            ? ['app', $word]
            : explode(':', $word, 2);
    }

    private function findWord(string $word, string $namespace): ?string
    {
        foreach ($this->orderProvider->getOrder()->getList() as $locale) {
            $translated = $this->translationProvider->get($namespace, $word, $locale);
            if ($translated !== null) {
                return $translated;
            }
        }

        return null;
    }

    public function getOrder(): LocaleOrderProviderInterface
    {
        return $this->orderProvider;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getSubscribedEvents(): iterable
    {
        yield 'request:900' => function () {
            LocalisedString::setLocalisator($this);
        };
    }
}
