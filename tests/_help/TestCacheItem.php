<?php
namespace Avris\Micrus\Test;

use Psr\Cache\CacheItemInterface;

final class TestCacheItem implements CacheItemInterface
{
    /** @var string */
    protected $key;

    /** @var bool */
    protected $hit;

    /** @var mixed */
    protected $value;

    public function __construct(string $key, bool $hit, $value = null)
    {
        $this->key = $key;
        $this->hit = $hit;
        $this->value = $value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function get()
    {
        return $this->isHit() ? $this->value : null;
    }

    public function isHit()
    {
        return $this->hit;
    }

    public function set($value)
    {
        $this->value = $value;
        $this->hit = true;

        return $this;
    }

    public function expiresAt($expiration)
    {
        return $this;
    }

    public function expiresAfter($time)
    {
        return $this;
    }
}
