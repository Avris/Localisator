<?php
namespace Avris\Localisator\Transformer\Selector;

use PHPUnit\Framework\TestCase;

/**
 * @cover \Avris\Localisator\Selector\CountVersion
 */
final class CountVersionTest extends TestCase
{
    /** @var CountVersionSelector */
    private $selector;

    protected function setUp()
    {
        $this->selector = new CountVersionSelector();
        $this->assertSame('', $this->selector->getName());
    }

    /**
     * @dataProvider selectProvider
     */
    public function testSelect($versions, $count, $expected)
    {
        $this->assertSame(
            $expected,
            $this->selector->select(['%count%' => $count], $versions)
        );
    }

    public function selectProvider()
    {
        return [
            [['jeden', 'wiele'], 1, 'jeden'],
            [['jeden', 'wiele'], 2, 'wiele'],
            [['jeden', 'wiele'], 3, 'wiele'],
            [['jeden', 'wiele'], 4, 'wiele'],
            [['jeden', 'wiele'], 5, 'wiele'],
            [['jeden', 'wiele'], 0, 'wiele'],
            [['jeden', 'wiele'], -1, 'jeden'],
            [['jeden', 'wiele'], -2, 'wiele'],

            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 1, 'pies'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 2, 'psy'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 3, 'psy'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 4, 'psy'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 5, 'psów'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 6, 'psów'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 7, 'psów'],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 8, ''],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], 0, ''],
            [['{1} pies', '{2,3,4} psy', '{5-7} psów'], -1, 'pies'],

            [['{-5} x', '{6} y', '{7-} z'], 0, 'x'],
            [['{-5} x', '{6} y', '{7-} z'], 1, 'x'],
            [['{-5} x', '{6} y', '{7-} z'], 5, 'x'],
            [['{-5} x', '{6} y', '{7-} z'], 6, 'y'],
            [['{-5} x', '{6} y', '{7-} z'], 7, 'z'],
            [['{-5} x', '{6} y', '{7-} z'], 8, 'z'],

            [['{5-7,19-22,25} A', '{} B'], 4, 'B'],
            [['{5-7,19-22,25} A', '{} B'], 5, 'A'],
            [['{5-7,19-22,25} A', '{} B'], 6, 'A'],
            [['{5-7,19-22,25} A', '{} B'], 7, 'A'],
            [['{5-7,19-22,25} A', '{} B'], 8, 'B'],
            [['{5-7,19-22,25} A', '{} B'], 18, 'B'],
            [['{5-7,19-22,25} A', '{} B'], 19, 'A'],
            [['{5-7,19-22,25} A', '{} B'], 25, 'A'],
            [['{5-7,19-22,25} A', '{} B'], 26, 'B'],
        ];
    }
}
