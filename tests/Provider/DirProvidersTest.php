<?php
namespace Avris\Localisator\Provider;

use Avris\Localisator\Provider\FileReader\JsonFileReader;
use Avris\Localisator\Provider\FileReader\PhpFileReader;
use Avris\Localisator\Provider\FileReader\YamlFileReader;
use Avris\Micrus\Test\TestModule;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Provider\DirsTranslationProvider
 * @covers \Avris\Localisator\Provider\ModuleDirTranslationProvider
 * @covers \Avris\Localisator\Provider\FileReader\PhpFileReader
 * @covers \Avris\Localisator\Provider\FileReader\YamlFileReader
 * @covers \Avris\Localisator\Provider\FileReader\JsonFileReader
 */
final class DirProvidersTest extends TestCase
{
    /** @var ModuleDirTranslationProvider */
    private $provider;

    protected function setUp()
    {
        $this->provider = new ModuleDirTranslationProvider(
            [
                new TestModule('module1', __DIR__ . '/../_help/module1'),
                new TestModule('module2', __DIR__ . '/../_help/module2'),
                new TestModule('module3', __DIR__ . '/../_help/module3'),
            ],
            [
                new PhpFileReader(),
                new YamlFileReader(),
                new JsonFileReader(),
            ]
        );
    }

    /**
     * @dataProvider getProvider
     */
    public function testGet(string $namespace, string $word, string $locale, ?string $expected)
    {
        $this->assertSame($expected, $this->provider->get($namespace, $word, $locale));
        $this->assertSame($expected, $this->provider->get($namespace, $word, $locale)); // cached in memory
    }

    public function getProvider()
    {
        yield ['app', 'foo.bar', 'en', 'Overwritten'];
        yield ['app', 'foo.bar', 'pl', 'Baż'];
        yield ['lol', 'foo.heh', 'en', 'OKEY'];
        yield ['lol', 'foo.json', 'pl', 'JSON'];
        yield ['lol', 'foo.heh', 'pl', null];
        yield ['assert', 'empty', 'en', 'This cannot be empty'];
        yield ['assert', 'empty', 'pl', 'To nie może być puste'];
        yield ['app', 'foo.bazinga', 'en', 'BAZINGA!'];
        yield ['app', 'foo.bazinga', 'pl', null];
        yield ['abc', 'def', 'en', null];
    }

    public function testKeys()
    {
        $this->assertEquals([
            'app:foo.bar',
            'assert:empty',
            'lol:foo.heh',
            'app:foo.bar',
            'assert:empty',
            'lol:foo.json',
        ], iterator_to_array($this->provider->keys()));
    }
}
