<?php
namespace Avris\Localisator\Provider;

use Avris\Bag\Bag;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Micrus\Test\TestCacheItem;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;

/**
 * @covers \Avris\Localisator\Provider\CacheTranslationProvider
 */
final class CacheTranslationProviderTest extends TestCase
{
    /** @var CacheTranslationProvider */
    private $provider;

    /** @var TranslationProviderInterface|MockObject */
    private $actualProvider;

    /** @var CacheItemPoolInterface|MockObject */
    private $cache;

    /** @var LocaleOrderProviderInterface|MockObject */
    private $orderProvider;

    protected function setUp()
    {
        $this->actualProvider = $this->getMockBuilder(TranslationProviderInterface::class)->getMock();
        $this->cache = $this->getMockBuilder(CacheItemPoolInterface::class)->getMock();
        $this->orderProvider = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();

        $this->provider = new CacheTranslationProvider(
            $this->actualProvider,
            $this->cache,
            $this->orderProvider
        );
    }

    public function testGetMiss()
    {
        $key = 'translation-de-app-foo.bar';

        $this->cache->expects($this->once())->method('getItem')->with($key)
            ->willReturn(new TestCacheItem($key, false));

        $this->actualProvider->expects($this->once())->method('get')
            ->with('app', 'foo.bar', 'de')->willReturn('OK');

        $this->cache->expects($this->once())->method('save')
            ->with(new TestCacheItem($key, true, 'OK'))->willReturn(true);

        $this->assertEquals(
            'OK',
            $this->provider->get('app', 'foo.bar', 'de')
        );
    }

    public function testGetHit()
    {
        $key = 'translation-de-app-foo.bar';

        $this->cache->expects($this->once())->method('getItem')->with($key)
            ->willReturn(new TestCacheItem($key, true, 'OK'));

        $this->actualProvider->expects($this->never())->method('get');

        $this->cache->expects($this->never())->method('save');

        $this->assertEquals(
            'OK',
            $this->provider->get('app', 'foo.bar', 'de')
        );
    }

    public function testKeys()
    {
        $this->actualProvider->expects($this->once())->method('keys')->willReturnCallback(function () {
            yield 'foo';
            yield 'bar';
        });

        $this->assertEquals(['foo', 'bar'], iterator_to_array($this->provider->keys()));
    }

    public function testWarmup()
    {
        $this->actualProvider->expects($this->once())->method('keys')->willReturnCallback(function () {
            yield 'app:foo';
            yield 'app:bar.ok';
            yield 'test:foo';
            yield 'test:lorem';
        });

        $this->orderProvider->expects($this->once())->method('getSupported')->willReturn(new Bag([
            'en_US' => 'English (US)',
            'pl' => 'Polski',
        ]));

        $this->cache->expects($this->exactly(12))->method('getItem')->withConsecutive(
            ['translation-en_US-app-foo'],
            ['translation-en-app-foo'],
            ['translation-pl-app-foo'],
            ['translation-en_US-app-bar.ok'],
            ['translation-en-app-bar.ok'],
            ['translation-pl-app-bar.ok'],
            ['translation-en_US-test-foo'],
            ['translation-en-test-foo'],
            ['translation-pl-test-foo'],
            ['translation-en_US-test-lorem'],
            ['translation-en-test-lorem'],
            ['translation-pl-test-lorem']
        )->willReturnCallback(function ($key) { return new TestCacheItem($key, false); });

        $this->actualProvider->expects($this->exactly(12))->method('get')->withConsecutive(
            ['app', 'foo', 'en_US'],
            ['app', 'foo', 'en'],
            ['app', 'foo', 'pl'],
            ['app', 'bar.ok', 'en_US'],
            ['app', 'bar.ok', 'en'],
            ['app', 'bar.ok', 'pl'],
            ['test', 'foo', 'en_US'],
            ['test', 'foo', 'en'],
            ['test', 'foo', 'pl'],
            ['test', 'lorem', 'en_US'],
            ['test', 'lorem', 'en'],
            ['test', 'lorem', 'pl']
        )->willReturnCallback(function ($ns, $word, $locale) { return $locale === 'en' ? strtoupper($word) : null; });

        $this->cache->expects($this->exactly(12))->method('saveDeferred')->withConsecutive(
            [new TestCacheItem('translation-en_US-app-foo', true, null)],
            [new TestCacheItem('translation-en-app-foo', true, 'FOO')],
            [new TestCacheItem('translation-pl-app-foo', true, null)],
            [new TestCacheItem('translation-en_US-app-bar.ok', true, null)],
            [new TestCacheItem('translation-en-app-bar.ok', true, 'BAR.OK')],
            [new TestCacheItem('translation-pl-app-bar.ok', true, null)],
            [new TestCacheItem('translation-en_US-test-foo', true, null)],
            [new TestCacheItem('translation-en-test-foo', true, 'FOO')],
            [new TestCacheItem('translation-pl-test-foo', true, null)],
            [new TestCacheItem('translation-en_US-test-lorem', true, null)],
            [new TestCacheItem('translation-en-test-lorem', true, 'LOREM')],
            [new TestCacheItem('translation-pl-test-lorem', true, null)]
        )->willReturn(true);

        $this->cache->expects($this->once())->method('commit')->willReturn(true);

        $this->provider->warmup();
    }
}
