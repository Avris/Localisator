<?php
namespace Avris\Localisator\String;

use Avris\Localisator\Localisator;
use Avris\Localisator\LocalisatorInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\String\LocalisedString
 */
class LocalisedStringTest extends TestCase
{
    public function testAll()
    {
        require_once __DIR__ . '/../../src/String/function.php';

        $localisator = $this->getMockBuilder(LocalisatorInterface::class)->disableOriginalConstructor()->getMock();
        $localisator->expects($this->any())->method('get')->willReturnCallback(function ($word, $replacements = []) {
            return $word === 'foo.bar'
                ? strtr('OK %value%', $replacements)
                : 'NOT FOUND';
        });
        $localisator->expects($this->any())->method('has')->willReturnCallback(function ($word, $replacements = []) {
            return $word === 'foo.bar';
        });

        $orgLocalisator = LocalisedString::getLocalisator();
        LocalisedString::setLocalisator($localisator);

        $ls = l('foo.bar', ['%value%' => 'YES']);
        $this->assertInstanceOf(LocalisedString::class, $ls);
        $this->assertSame('OK YES', $ls->getLocalised());
        $this->assertSame('OK YES', (string) $ls);
        $this->assertSame('"OK YES"', json_encode($ls));
        $this->assertTrue($ls->exists());

        $ls = l('nope', ['%value%' => 'YES']);
        $this->assertInstanceOf(LocalisedString::class, $ls);
        $this->assertSame('NOT FOUND', $ls->getLocalised());
        $this->assertSame('NOT FOUND', (string) $ls);
        $this->assertSame('"NOT FOUND"', json_encode($ls));
        $this->assertFalse($ls->exists());

        LocalisedString::setLocalisator(null);

        $ls = l('Value is %value%', ['%value%' => '5']);
        $this->assertInstanceOf(LocalisedString::class, $ls);
        $this->assertSame('Value is 5', $ls->getLocalised());
        $this->assertSame('Value is 5', (string) $ls);
        $this->assertSame('"Value is 5"', json_encode($ls));
        $this->assertTrue($ls->exists());

        LocalisedString::setLocalisator($orgLocalisator);
    }
}
