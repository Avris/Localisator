<?php
namespace Avris\Localisator\Locale;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Locale\Locale
 */
class LocaleTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testData($string, $language, $country, $isGeneral)
    {
        $locale = new Locale($string);
        $this->assertSame($string, (string) $locale);
        $this->assertSame($language, $locale->getLanguage());
        $this->assertSame($country, $locale->getCountry());
        $this->assertSame($isGeneral, $locale->isGeneral());
    }

    public function dataProvider()
    {
        return [
            ['de', 'de', null, true],
            ['de_AT', 'de', 'AT', false],
        ];
    }
}
