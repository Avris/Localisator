<?php
namespace Avris\Localisator\Handler;

use Avris\Bag\Bag;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Http\Header\HeaderBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Handler\SessionLocaleHandler
 * @covers \Avris\Localisator\Handler\LocaleChangedEvent
 */
final class SessionLocaleHandlerTest extends TestCase
{
    /**
     * @dataProvider provider
     */
    public function testSwitchLocale(
        array $referrer,
        ?string $current,
        string $setTo,
        array $eventResult,
        ?string $new,
        $redirect
    )
    {
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->expects($this->once())->method('getHeaders')->willReturn(new HeaderBag($referrer));

        $session = $this->getMockBuilder(SessionInterface::class)->getMock();
        $session->expects($this->once())->method('get')->with('_locale')->willReturn($current);
        if ($new) {
            $session->expects($this->once())->method('set')->with('_locale', $new);
        }

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router->expects(count($referrer) ? $this->never() : $this->once())
            ->method('generate')->with('home')->willReturn('/');

        $order = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();
        $order->expects($this->once())->method('getSupported')->willReturn(new Bag([
            'de' => 'Deutsch',
            'nl' => 'Nederlands',
        ]));

        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();
        $dispatcher->expects($this->once())->method('trigger')
            ->with(new LocaleChangedEvent($current, $setTo))
            ->willReturn($eventResult);

        $handler = new SessionLocaleHandler();
        $response = $handler->changeLocaleAction(
            $request,
            $session,
            $router,
            $order,
            $dispatcher,
            $setTo
        );
        $this->assertEquals(new RedirectResponse($redirect), $response);
    }

    public function provider()
    {
        yield [[], 'de', 'nl', [true, 'nl'], 'nl', '/'];
        yield [[], null, 'nl', [true, 'nl'], 'nl', '/'];
        yield [['referer' => '/foo'], 'de', 'nl', [true, 'nl'], 'nl', '/foo'];
        yield [[], 'de', 'nl', [false, 'nl'], null, '/'];
        yield [[], null, 'nl', [true, 'de'], 'de', '/'];
    }

    /**
     * @expectedException \Avris\Micrus\Exception\Http\NotFoundHttpException
     * @expectedExceptionMessage Unsupported locale fr
     */
    public function testSwitchLocaleUnsupported()
    {
        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->expects($this->never())->method('getHeaders');

        $session = $this->getMockBuilder(SessionInterface::class)->getMock();
        $session->expects($this->never())->method('get');
        $session->expects($this->never())->method('set');

        $router = $this->getMockBuilder(RouterInterface::class)->getMock();
        $router->expects($this->never())->method('generate');

        $order = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();
        $order->expects($this->once())->method('getSupported')->willReturn(new Bag([
            'de' => 'Deutsch',
            'nl' => 'Nederlands',
        ]));

        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();
        $dispatcher->expects($this->never())->method('trigger');

        $handler = new SessionLocaleHandler();
        $handler->changeLocaleAction(
            $request,
            $session,
            $router,
            $order,
            $dispatcher,
            'fr'
        );
    }

    public function testEvent()
    {
        $event = new LocaleChangedEvent(null, 'de');
        $this->assertEquals('localeChanged', $event->getName());
        $this->assertNull($event->getOldLocale());
        $this->assertEquals('de', $event->getNewLocale());
        $this->assertTrue($event->shouldChange());
        $this->assertSame([true, 'de'], $event->getValue());

        $this->assertSame($event, $event->setShouldChange(false));
        $this->assertSame($event, $event->setNewLocale('pl'));
        $this->assertSame([false, 'pl'], $event->getValue());

        $this->assertSame($event, $event->setValue([true, 'nl']));
        $this->assertSame([true, 'nl'], $event->getValue());
    }
}
