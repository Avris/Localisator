<?php
namespace Avris\Localisator\Handler;

use Avris\Bag\Bag;
use Avris\Container\Container;
use Avris\Container\ContainerInterface;
use Avris\Http\Header\HeaderBag;
use Avris\Http\Response\RedirectResponse;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use Avris\Micrus\Controller\Routing\Service\RouteCompiler;
use Avris\Micrus\Controller\Routing\Service\RouteGenerator;
use Avris\Micrus\Controller\Routing\Service\RouteParser;
use Avris\Micrus\Controller\Routing\Service\Router;
use Avris\Micrus\Controller\Routing\Service\RouterInterface;
use Avris\Localisator\Locale\Locale;
use Avris\Localisator\Order\LocaleOrder;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Micrus\Tool\Cache\CacherInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

/**
 * @covers \Avris\Localisator\Handler\UrlLocaleHandler
 */
final class UrlLocaleHandlerTest extends TestCase
{
    /** @var ContainerInterface */
    private $container;

    /** @var RouterInterface */
    private $router;

    /** @var UrlLocaleHandler */
    private $handler;

    protected function setUp()
    {
        $order = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();
        $order->expects($this->any())->method('getCurrent')->willReturn(new Locale('nl'));

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();

        $requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($routeMatch);

        $this->container = new Container();
        $this->container->set(LocaleOrderProviderInterface::class, $order);
        $this->container->set(RequestProviderInterface::class, $requestProvider);

        $this->handler = new UrlLocaleHandler(
            $this->container,
            new Bag(['en' => 'English', 'pl' => 'Polski', 'nl' => 'Nederlands'])
        );

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->expects($this->any())->method('getBase')->willReturn('');
        $request->expects($this->any())->method('getAbsoluteBase')->willReturn('');
        $request->expects($this->any())->method('getFrontController')->willReturn('');

        $cacher = $this->getMockBuilder(CacherInterface::class)->getMock();
        $cacher->expects($this->any())->method('cache')->willReturnCallback(function ($name, callable $callback) {
            return $callback();
        });

        $logger = $this->getMockBuilder(LoggerInterface::class)->getMock();

        $this->router = new Router(
            $request,
            $cacher,
            $logger,
            [$this->handler],
            new Bag([
                'home' => '/ -> home/home',
                'foo' => '/foo -> foo/foo',
                'bar' => '/bar/{id} -> bar/bar',
                'determineLocale' => '/ -> ' . UrlLocaleHandler::class. '/determineLocale {"immutable":true}',
            ]),
            new RouteParser(),
            new RouteCompiler(),
            new RouteGenerator()
        );

        $routeMatch->expects($this->any())->method('getRoute')->willReturn($this->router->getRoutes()['bar']);
        $routeMatch->expects($this->any())->method('getTags')->willReturn(['id' => 8]);

        $this->container->set(RouterInterface::class, $this->router);
    }

    public function testAdd()
    {
        $routes = $this->router->getRoutes();

        $this->assertEquals('/{_locale}', $routes['home']->getPattern());
        $this->assertEquals('/{_locale}/foo', $routes['foo']->getPattern());
        $this->assertEquals('/{_locale}/bar/{id}', $routes['bar']->getPattern());
        $this->assertEquals('/', $routes['determineLocale']->getPattern());

        $this->assertEquals(['_locale' => 'en|pl|nl'], $routes['home']->getRequirements());
    }

    public function testGenerate()
    {
        $this->assertEquals('/nl', $this->router->generate('home'));
        $this->assertEquals('/nl/foo', $this->router->generate('foo'));
        $this->assertEquals('/nl/bar/5', $this->router->generate('bar', ['id' => 5]));
        $this->assertEquals('/', $this->router->generate('determineLocale'));

        $this->assertEquals('/pl/foo', $this->router->generate('foo', ['_locale' => 'pl']));
    }

    public function testGenerateChange()
    {
        $this->assertEquals('/pl/bar/8', $this->router->generate('changeLocale', ['locale' => 'pl']));
    }

    public function testGenerateOn404()
    {
        $requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $requestProvider->expects($this->any())->method('getRouteMatch')->willReturn(null);
        $this->container->set(RequestProviderInterface::class, $requestProvider);

        $this->assertEquals('/pl', $this->router->generate('changeLocale', ['locale' => 'pl']));
    }

    public function testDetermineLocaleAction()
    {
        $headers = new HeaderBag([
            'accept-language' => 'it,en;q=0.9',
        ]);

        $request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $request->expects($this->once())->method('getHeaders')->willReturn($headers);

        $supported = new Bag(['en' => 'English', 'es' => 'Español']);

        $order = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();
        $order->expects($this->once())->method('getSupported')
            ->willReturn($supported);
        $order->expects($this->once())->method('getOrder')->willReturn(new LocaleOrder($supported, ['es']));

        $response = $this->handler->determineLocaleAction(
            $request,
            $this->router,
            $order
        );

        $this->assertEquals(new RedirectResponse('/en'), $response);
    }
}
