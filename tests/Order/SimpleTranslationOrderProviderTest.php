<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Order\SimpleLocaleOrderProvider
 */
final class SimpleTranslationOrderProviderTest extends TestCase
{
    public function testResolveFull()
    {
        $supported = new Bag(['es' => 'es']);
        $order = new LocaleOrder($supported, ['es']);

        $provider = new SimpleLocaleOrderProvider('es');

        $this->assertEquals($order, $provider->getOrder());
        $this->assertEquals($order, $provider->getOrder());

        $this->assertEquals($supported, $provider->getSupported());
        $this->assertEquals('es', (string) $provider->getCurrent());
    }
}
