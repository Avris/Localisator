<?php
namespace Avris\Localisator\Order;

use Avris\Bag\Bag;
use Avris\Http\Header\HeaderBag;
use Avris\Http\Header\QualityHeader;
use Avris\Http\Request\RequestInterface;
use Avris\Micrus\Controller\RequestProviderInterface;
use Avris\Http\Session\SessionInterface;
use Avris\Micrus\Controller\Routing\Model\RouteMatch;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Order\MicrusLocaleOrderProvider
 */
final class MicrusTranslationOrderProviderTest extends TestCase
{
    /** @var RequestProviderInterface|MockObject */
    private $requestProvider;

    /** @var RequestInterface|MockObject */
    private $request;

    /** @var HeaderBag|MockObject */
    private $headers;

    /** @var SessionInterface|MockObject */
    private $session;

    protected function setUp()
    {
        $this->headers = $this->getMockBuilder(HeaderBag::class)->disableOriginalConstructor()->getMock();

        $this->request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $this->request->expects($this->any())->method('getHeaders')->willReturn($this->headers);

        $this->requestProvider = $this->getMockBuilder(RequestProviderInterface::class)->getMock();
        $this->requestProvider->expects($this->any())->method('getRequest')->willReturn($this->request);

        $this->session = $this->getMockBuilder(SessionInterface::class)->getMock();
    }

    public function testEmpty()
    {
        $this->session->expects($this->any())->method('get')->with('_locale')->willReturn(null);

        $header = $this->getMockBuilder(QualityHeader::class)->disableOriginalConstructor()->getMock();
        $header->expects($this->once())->method('getSorted')->willReturn([]);
        $this->headers->expects($this->once())->method('getLanguage')->willReturn($header);

        $supported = new Bag();

        $provider = new MicrusLocaleOrderProvider(
            $this->requestProvider,
            $this->session,
            $supported
        );

        $provider->rebuild();
        $order = $provider->getOrder();

        $this->assertInstanceOf(LocaleOrder::class, $order);
        $this->assertSame(['en'], $order->getList());

        $this->assertSame($supported, $provider->getSupported());
        $this->assertSame('en', (string) $provider->getCurrent());
    }

    public function testFull()
    {
        $this->session->expects($this->any())->method('get')->with('_locale')->willReturn('it');

        $routeMatch = $this->getMockBuilder(RouteMatch::class)->disableOriginalConstructor()->getMock();
        $routeMatch->expects($this->any())->method('getTags')->willReturn(['_locale' => 'de']);
        $this->requestProvider->expects($this->any())->method('getRouteMatch')->willReturn($routeMatch);

        $header = $this->getMockBuilder(QualityHeader::class)->disableOriginalConstructor()->getMock();
        $header->expects($this->any())->method('getSorted')->willReturn(['pl', 'en']);
        $this->headers->expects($this->any())->method('getLanguage')->willReturn($header);

        $supported = new Bag([
            'en' => 'English',
            'de' => 'Deutsch',
            'it' => 'Italiano',
            'pl' => 'Polski',
            'es' => 'Español',
            'ru' => 'Русский',
        ]);

        $provider = new MicrusLocaleOrderProvider(
            $this->requestProvider,
            $this->session,
            $supported,
            'es'
        );

        $provider->rebuild();
        $order = $provider->getOrder();

        $this->assertInstanceOf(LocaleOrder::class, $order);
        $this->assertSame(['de', 'it', 'pl', 'en', 'es'], $order->getList());
        $this->assertSame($supported, $provider->getSupported());
        $this->assertSame('de', (string) $provider->getCurrent());

        $provider->rebuild('ru');
        $order = $provider->getOrder();

        $this->assertInstanceOf(LocaleOrder::class, $order);
        $this->assertSame(['ru', 'de', 'it', 'pl', 'en', 'es'], $order->getList());
        $this->assertSame($supported, $provider->getSupported());
        $this->assertSame('ru', (string) $provider->getCurrent());

        $provider->rebuild();
        $order = $provider->getOrder();

        $this->assertInstanceOf(LocaleOrder::class, $order);
        $this->assertSame(['de', 'it', 'pl', 'en', 'es'], $order->getList());
        $this->assertSame($supported, $provider->getSupported());
        $this->assertSame('de', (string) $provider->getCurrent());
    }
}
