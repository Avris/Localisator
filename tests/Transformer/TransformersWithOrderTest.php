<?php
namespace Avris\Micrus\Transformer;

use Avris\Dispatcher\EventDispatcher;
use Avris\Localisator\LocalisatorInterface;
use Avris\Localisator\Transformer\Selector\CountVersionSelector;
use Avris\Localisator\Transformer\FallbackToPatternTransformer;
use Avris\Localisator\Transformer\FallbackToWordTransformer;
use Avris\Localisator\Transformer\NestedTransformer;
use Avris\Localisator\Transformer\ReplacementsTransformer;
use Avris\Localisator\Transformer\SelectorsTransformer;
use Avris\Localisator\Transformer\TranslationTransformEvent;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Transformer\AbstractTransformer
 * @covers \Avris\Localisator\Transformer\FallbackToPatternTransformer
 * @covers \Avris\Localisator\Transformer\FallbackToWordTransformer
 * @covers \Avris\Localisator\Transformer\NestedTransformer
 * @covers \Avris\Localisator\Transformer\SelectorsTransformer
 * @covers \Avris\Localisator\Transformer\ReplacementsTransformer
 *
 * @covers \Avris\Localisator\Transformer\TranslationTransformEvent
 */
final class TransformersWithOrderTest extends TestCase
{
    /** @var EventDispatcher */
    private $dispatcher;

    /** @var bool */
    private $wordFallbackAttached = false;

    protected function setUp()
    {
        $localisator = $this->getMockBuilder(LocalisatorInterface::class)->getMock();
        $localisator->expects($this->any())->method('get')->willReturnCallback(
            function (string $word, array $replacements = []) {
                return $word === 'social.facebook' ? 'Facebook' : null;
            }
        );

        $this->dispatcher = new EventDispatcher();
        $this->dispatcher->registerSubscriber(new NestedTransformer($localisator));
        $this->dispatcher->registerSubscriber(new SelectorsTransformer([new CountVersionSelector()]));
        $this->dispatcher->registerSubscriber(new FallbackToPatternTransformer(['entity' => ['^(.*)\.singular$']]));
        $this->dispatcher->registerSubscriber(new ReplacementsTransformer());
    }

    /**
     * @dataProvider provider
     */
    public function testTransform(
        string $namespace,
        string $word,
        array $replacements,
        ?string $translated,
        ?string $expected,
        $attachWordFallback = false
    )
    {
        if ($attachWordFallback && !$this->wordFallbackAttached) {
            $this->dispatcher->registerSubscriber(new FallbackToWordTransformer());
            $this->wordFallbackAttached = false;
        }

        $event = new TranslationTransformEvent($namespace, $word, $replacements, $translated);
        $transformed = $this->dispatcher->trigger($event);
        $this->assertSame($expected, $transformed);
    }

    public function provider()
    {
        // empty
        yield ['app', 'foo', [], 'No transformations', 'No transformations'];
        yield ['app', 'foo', [], null, null];

        // nested
        yield ['app', 'foo', [], 'Zaloguj przez [[social.facebook]]', 'Zaloguj przez Facebook'];
        yield ['app', 'foo', ['source' => 'facebook'], 'Zaloguj przez [[social.%source%]]', 'Zaloguj przez Facebook'];
        yield ['app', 'foo', [], 'Zaloguj przez [[social.nope]]', 'Zaloguj przez '];
        yield ['app', 'foo', ['source' => 'nope'], 'Zaloguj przez [[social.%source%]]', 'Zaloguj przez '];

        // Replacements
        yield ['app', 'foo', ['lorem' => 'Ipsum'], 'Lorem is %lorem%', 'Lorem is Ipsum'];
        yield ['app', 'foo', ['%lorem%' => 'Ipsum'], 'Lorem is %lorem%', 'Lorem is Ipsum'];
        yield ['app', 'foo', [], 'Lorem is %lorem%', 'Lorem is %lorem%'];
        
        // Selectors
        yield ['app', 'foo', ['count' => 1], '<> dog|dogs', 'dog'];
        yield ['app', 'foo', ['count' => 5], '<> dog|dogs', 'dogs'];
        yield ['app', 'foo', ['count' => 1], '<> a dog|%count% dogs', 'a dog'];
        yield ['app', 'foo', ['count' => 5], '<> a dog|%count% dogs', '5 dogs'];

        // Fallback to pattern
        yield ['entity', 'User.singular', [], null, 'User'];

        // Fallback to word
        yield ['app', 'nope.none', [], null, 'nope.none', true];
        yield ['app', 'nope.none', [], 'found anyway', 'found anyway', true];
    }

    public function testEvent()
    {
        $event = new TranslationTransformEvent('ns', 'foo', ['lorem' => 'ipsum'], null);
        $this->assertEquals('translationTransform', $event->getName());
        $this->assertEquals('ns', $event->getNamespace());
        $this->assertEquals('foo', $event->getWord());
        $this->assertEquals(['%lorem%' => 'ipsum'], $event->getReplacements());
        $this->assertNull($event->getTranslated());
        $this->assertFalse($event->hasTranslation());

        $this->assertSame($event, $event->setNamespace('app'));
        $this->assertEquals('app', $event->getNamespace());
        $this->assertSame($event, $event->setWord('bar'));
        $this->assertEquals('bar', $event->getWord());
        $this->assertSame($event, $event->setReplacements(['lorem' => 'll']));
        $this->assertEquals(['%lorem%' => 'll'], $event->getReplacements());
        $this->assertSame($event, $event->setTranslated('trrr'));
        $this->assertEquals('trrr', $event->getTranslated());
        $this->assertTrue($event->hasTranslation());
    }
}
