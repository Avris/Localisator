<?php
namespace Avris\Localisator;

use Avris\Bag\Bag;
use Avris\Dispatcher\EventDispatcherInterface;
use Avris\Localisator\Locale\Locale;
use Avris\Localisator\Order\LocaleOrder;
use Avris\Localisator\Order\LocaleOrderProviderInterface;
use Avris\Localisator\Provider\TranslationProviderInterface;
use Avris\Localisator\Transformer\TranslationTransformEvent;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Localisator\Localisator
 */
final class LocalisatorTest extends TestCase
{
    const DATA = [
        'en' => [
            'app' => [
                'foo' => 'bar',
                'lorem' => 'ipsum',
            ],
            'test' => [
                'foo' => 'TST',
            ],
        ],
        'pl' => [
            'app' => [
                'foo' => 'fóó',
            ],
        ]
    ];

    const REPLACEMENTS = ['a' => 'b'];

    /**
     * @dataProvider provider
     */
    public function testGet(array $order, string $input, string $namespace, string $word, ?string $expected)
    {
        $orderProvider = $this->getMockBuilder(LocaleOrderProviderInterface::class)->getMock();
        $orderProvider->expects($this->any())->method('getOrder')
            ->willReturn(new LocaleOrder(new Bag($order), array_map(function (string $locale) {
                return new Locale($locale);
            }, $order)));

        $translationProvider = $this->getMockBuilder(TranslationProviderInterface::class)->getMock();
        $translationProvider->expects($this->any())->method('get')
            ->willReturnCallback(function (string $namespace, string $word, string $locale): ?string {
                return self::DATA[$locale][$namespace][$word] ?? null;
            });

        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();
        $dispatcher->expects($this->any())->method('trigger')
            ->with(new TranslationTransformEvent($namespace, $word, self::REPLACEMENTS, $expected))
            ->willReturnCallback(function (TranslationTransformEvent $event) use ($expected) {
                if ($event->hasTranslation()) {
                    return $expected . '-ext';
                }
            });

        $localisator = new Localisator(
            $translationProvider,
            $orderProvider,
            $dispatcher
        );

        $this->assertSame($orderProvider, $localisator->getOrder());

        if ($expected === null) {
            $this->assertNull($localisator->get($input, self::REPLACEMENTS));
            $this->assertNull($localisator($input, self::REPLACEMENTS));
            $this->assertFalse($localisator->has($input));
        } else {
            $this->assertEquals($expected . '-ext', $localisator->get($input, self::REPLACEMENTS));
            $this->assertEquals($expected . '-ext', $localisator($input, self::REPLACEMENTS));
            $this->assertTrue($localisator->has($input));
        }
    }

    public function provider()
    {
        yield [['en', 'pl'], 'foo', 'app', 'foo', 'bar'];
        yield [['en', 'pl'], 'app:foo', 'app', 'foo', 'bar'];
        yield [['pl'], 'foo', 'app', 'foo', 'fóó'];
        yield [['pl', 'en'], 'lorem', 'app', 'lorem', 'ipsum'];
        yield [['pl'], 'lorem', 'app', 'lorem', null];
        yield [['en', 'pl'], 'test:foo', 'test', 'foo', 'TST'];
        yield [[], 'foo', 'app', 'foo', null];
        yield [['en', 'pl'], 'osiem', 'app', 'osiem', null];
    }
}
